import com.sun.deploy.util.SessionState;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import java.math.BigInteger;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by LardonCrevette on 23/10/2015.
 */
public class main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration().configure();
        configuration.addResource("hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();
        //Transaction transaction;

        //transaction = session.beginTransaction();
        //transaction.commit();

        List<Client2> list = session.createCriteria(Client2.class)
                .add(Restrictions.like("nom", "LORIO")).add(Restrictions.like("prenom","Didier")).list();

        for(Client2 part : list){
            System.out.println(part);

            Set<Compte> ppp = part.getCompte();
            for(Iterator<Compte> it = ppp.iterator(); it.hasNext(); ){
                System.out.println(it.next());

                Set<Credit> credits = it.next().getCredit();
                for (Iterator<Credit> cit = credits.iterator(); cit.hasNext(); ){
                    System.out.println(cit.next());
                }
            }
        }
    }
}
