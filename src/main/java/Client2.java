import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by LardonCrevette on 23/10/2015.
 */
@Entity
@Table(name = "client_test")
public class Client2 {

    public Client2() {
    }

    public Client2(Set<Compte> compte, Date derniereConnexion, String login, String motDePasse, String nom, String prenom, String profil, int version) {
        this.compte = compte;
        this.derniereConnexion = derniereConnexion;
        this.login = login;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.profil = profil;
        this.version = version;
    }

    @Id
    @GeneratedValue
    @Column(name = "clt_id", nullable = false, unique = true)
    private int id;

    @Column (name = "clt_derniere_connexion")
    private Date derniereConnexion;

    @Column (name = "clt_login")
    private String login;

    @Column (name = "clt_mot_de_passe")
    private String motDePasse;

    @Column (name = "clt_nom")
    private String nom;

    @Column (name = "clt_prenom")
    private String prenom;

    @Column (name = "clt_profil")
    private String profil;

    @Column (name = "clt_version")
    private int version;

    @OneToMany (mappedBy = "client")
    private Set<Compte> compte;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDerniereConnexion() {
        return derniereConnexion;
    }

    public void setDerniereConnexion(Date derniereConnexion) {
        this.derniereConnexion = derniereConnexion;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Set<Compte> getCompte() {
        return compte;
    }

    public void setCompte(Set<Compte> compte) {
        this.compte = compte;
    }

    @Override
    public String toString(){
        return this.nom + " " + this.prenom
                + " (Login: " +this.login
                + ", Mdp: " + this.motDePasse
                + ", Connect: " + this.derniereConnexion
                + ", Id: " + this.id
                + ", Vers: " + this.version
                + ", Profil: " + this.profil + ")";
    }
}
