import javax.persistence.*;
import java.math.BigInteger;
import java.util.Set;

/**
 * Created by LardonCrevette on 23/10/2015.
 */

@Entity
@Table (name = "compte_test")
public class Compte {

    @Column (name = "coe_discriminant", length = 31)
    private String discriminant;

    @Id @GeneratedValue
    @Column (name = "coe_id", nullable = false, unique = true)
    private int id;

    @Column (name = "coe_description")
    private String description;

    @Column (name = "coe_numero")
    private String numero;

    @Column (name = "coe_solde")
    private Double solde;

    @Column (name = "coe_decouvert_autorise")
    private Double decouvertAutorise;

    @Column (name = "coe_plafond")
    private Double plafond;

    @Column (name = "coe_taux")
    private Double taux;

    @ManyToOne
    @JoinColumn(name = "clt_id")
    private Client2 client;

    @ManyToMany
    @JoinTable(name = "credit_compte_test" , joinColumns = {@JoinColumn(name = "coe_id")}, inverseJoinColumns = {@JoinColumn(name = "crt_id")})
    private Set<Credit> credit;


    public Compte() {
    }

    public Compte(String discriminant, String description, String numero, Double solde, Double decouvertAutorise, Double plafond, Double taux, Client2 client) {
        this.discriminant = discriminant;
        this.description = description;
        this.numero = numero;
        this.solde = solde;
        this.decouvertAutorise = decouvertAutorise;
        this.plafond = plafond;
        this.taux = taux;
        this.client = client;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiscriminant() {
        return discriminant;
    }

    public void setDiscriminant(String discriminant) {
        this.discriminant = discriminant;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Double getSolde() {
        return solde;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public Double getDecouvertAutorise() {
        return decouvertAutorise;
    }

    public void setDecouvertAutorise(Double decouvertAutorise) {
        this.decouvertAutorise = decouvertAutorise;
    }

    public Double getPlafond() {
        return plafond;
    }

    public void setPlafond(Double plafond) {
        this.plafond = plafond;
    }

    public Double getTaux() {
        return taux;
    }

    public void setTaux(Double taux) {
        this.taux = taux;
    }

    public Client2 getClient() {
        return client;
    }

    public void setClient(Client2 client) {
        this.client = client;
    }

    public Set<Credit> getCredit() {
        return credit;
    }

    public void setCredit(Set<Credit> credit) {
        this.credit = credit;
    }

    @Override
    public String toString(){
        return "(Disc: " + this.discriminant
                + ", Desc: " + this.description
                + ", Num: " + this.numero
                + ", Solde: " + this.solde
                + ", Dec: " + this.decouvertAutorise
                + ", Plaf: " + this.plafond
                + ", Taux: " + this.taux + ")";
    }
}
