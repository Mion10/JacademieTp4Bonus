import javax.persistence.*;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by LardonCrevette on 23/10/2015.
 */
@Entity
@Table (name = "credit_test")
public class Credit {
    @Id
    @GeneratedValue
    @Column (name = "crt_id", nullable = false, unique = true)
    private int id;

    @Column(name = "crt_description")
    private String description;

    @Column(name = "crt_montant")
    private String montant;

    @Column(name = "crt_taux")
    private String taux;

    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable(name = "credit_compte_test" , joinColumns = {@JoinColumn(name = "crt_id")}, inverseJoinColumns = {@JoinColumn(name = "coe_id")})
    private Set<Compte> crt_compte;


    public Credit() {
    }

    public Credit(String description, String montant, String taux) {
        this.description = description;
        this.montant = montant;
        this.taux = taux;
    }

    public Set<Compte> getCrt_compte() {
        return crt_compte;
    }

    public void setCrt_compte(Set<Compte> crt_compte) {
        this.crt_compte = crt_compte;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMontant() {
        return montant;
    }

    public void setMontant(String montant) {
        this.montant = montant;
    }

    public String getTaux() {
        return taux;
    }

    public void setTaux(String taux) {
        this.taux = taux;
    }

    @Override
    public String toString(){
        return "(Desc: " + this.description
            + ", Mont: " + this.montant
            + ", Taux: " + this.taux + ")";
    }
}
